
var Regex = {

   validateEmail:function(val){
       return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val);
   },


   validateMobile:function(text){
      return /^\+(?:[0-9] ?){6,14}[0-9]$/.test(text);
   },


   validateString:function(val){
   	return /[^a-zA-Z]+/.test(val);
   },

   validateStringMinimumLength2:function(val){
      return /^[a-zA-Z\x20]{2,25}$/.test(val);
   },

   validateNumbers:function(val){
      return /^[0-9]{0,}$/.test(val);
   },

   validateURL:function(url){
      return /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/.test(url);
   },


}

module.exports = Regex;
