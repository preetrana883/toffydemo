"use strict"

let colors = {
    Transparent: 'transparent',
    White:"#ffffff",
    Gray: "#737373",
    ButtonColor:"#bc1818",
    Black:'#212123',
    PureBlack : "#000000",
    PrimaryColor: '#FF4571',
    HeaderGreen: "#1e980a",
};

module.exports =  colors;
