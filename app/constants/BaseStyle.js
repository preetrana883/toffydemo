'use-strict';

import {React,PixelRatio} from "react-native";
import Dimensions from 'Dimensions';

// Precalculate Device Dimensions for better performance
const x = Dimensions.get('window')
    .width;
const y = Dimensions.get('window')
    .height;

// Then we set our styles with the help of the em() function
const BaseStyle = {

    // GENERAL
    DEVICE_WIDTH: x,
    DEVICE_HEIGHT: y,

};

module.exports = BaseStyle;
