'use strict';

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity
} from 'react-native';

import Constants from '../../constants';

export default class CircleStatus extends Component{
  // Default render function
  render(){

    let { text, onPress} = this.props;

    return(
      <TouchableOpacity onPress={onPress} style={[styles.container,{borderColor: this.props.selectedTitle ? "rgb(155,227,233)": Constants.Colors.Black}]}>
        <Text>{text}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
    container:{
      alignItems: 'center',
      borderWidth:1,
      marginLeft: 20,
      width: Constants.BaseStyle.DEVICE_WIDTH/100*16,
      borderRadius: Constants.BaseStyle.DEVICE_WIDTH/100*8,
      height: Constants.BaseStyle.DEVICE_WIDTH/100*16,
      justifyContent: 'center'
    }
});
