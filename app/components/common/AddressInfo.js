'use strict';

import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput
} from 'react-native';

import Constants from '../../constants';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class AddressInfo extends Component {
    // Default render function
    render() {

        let { onChangeText, value, maxLength,
            placeholder, keyboardType, pincode, mobile,city, onNextPress, width } = this.props;

        return (
            <View style={styles.container}>
                <TextInput
                    style={[styles.inputStyle, { width: width }]}
                    onChangeText={onChangeText}
                    value={value}
                    maxLength={maxLength}
                    keyboardType={keyboardType}
                    placeholder={placeholder}
                />
                {pincode && <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: 'center',position:'absolute', right:Constants.BaseStyle.DEVICE_WIDTH/100*2, top:Constants.BaseStyle.DEVICE_HEIGHT/100*3 }}>
                    <Image source={Constants.Images.user.location}
                        style={styles.imgStyleBack} />
                    <Text style={styles.locateMeText}>Locate me</Text>
                </View>}

                {city && <View style={styles.cityFieldText}>
                    <Image source={Constants.Images.user.downArrow}
                        style={styles.imgStyleBack} />
                    <TouchableOpacity onPress={onNextPress}>
                        <Image source={Constants.Images.user.rightArrow}
                            style={styles.imgStyleBack} />
                    </TouchableOpacity>

                </View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 80,
        padding: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 1.5,
        borderBottomWidth: 1,
        //justifyContent: 'space-between',
    },
    locateMeText: {
        fontSize: 16,
        color: 'gray',
        marginLeft: 6
    },
    imgStyleBack: {
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 5,
        height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 3,
        resizeMode:'contain'
    },
    inputStyle: {
        borderColor: 'gray',
        fontSize: 16,
    },
    imgStyle: {
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 12,
        height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 12,
        borderRadius: Constants.BaseStyle.DEVICE_WIDTH / 100 * 6,
        borderWidth: 1
    },
    textStyle: {
        color: Constants.Colors.Black,
        fontSize: 16,
        fontWeight: "400",
        //marginLeft: 10,
    },
    DengueSecondText: {
        color: Constants.Colors.White,
        fontSize: 16,
        fontWeight: "300",
        textAlign: "center",
    },
    DengueText: {
        color: Constants.Colors.White,
        fontSize: 20,
        fontWeight: "500"
    },
    duratinText: {
        color: Constants.Colors.White,
        fontSize: 14
    },
    cityFieldText: {
        flexDirection: "row",
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 30,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginLeft:Constants.BaseStyle.DEVICE_WIDTH/100*7
    }
});
