'use strict';

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image
} from 'react-native';

import Constants from '../../constants';

export default class policyTextCompo extends Component{
  // Default render function
  render(){

    let { firstText, secondText} = this.props;

    return(
      <View style={styles.container}>
        <View>
          <Text style={styles.DengueText}>{firstText}</Text>
          <Text style={styles.duratinText}>{secondText}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    //flexDirection: 'row',
    alignItems:'center',
    justifyContent: 'space-between',
    //marginBottom: Constants.BaseStyle.DEVICE_WIDTH / 100 * 3,

  },
    DengueText: {
      color: Constants.Colors.White,
      fontSize: 20,
      fontWeight: "500"
    },
    duratinText: {
      color: Constants.Colors.White,
      fontSize: 14
    },
    imgStyleBack: {
      width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 7,
      height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4,
      resizeMode: 'contain'
    },
});
