'use strict';

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View
} from 'react-native';

import Constants from '../../constants';

export default class PolicydetailsInfo extends Component{
  // Default render function
  render(){

    let { firstText, secondText} = this.props;

    return(
      <View>
        <Text style={styles.DengueText}>{firstText}</Text>
        <Text style={styles.DengueSecondText}>{secondText}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    DengueText:{
        color: Constants.Colors.White,
        fontSize: 18,
        fontWeight: "600"
    },
    DengueSecondText:{
        color: Constants.Colors.White,
        fontSize: 16,
        fontWeight: "300",
        textAlign: "center",
    },
});
