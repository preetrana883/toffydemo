'use strict';

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image
} from 'react-native';

import Constants from '../../constants';

export default class policyTextCompo extends Component{
  // Default render function
  render(){

    let { source, text, color} = this.props;

    return(
      <View style={styles.container}>
        <Image source={source} style={styles.imgStyle}/>
        <Text style={[styles.textStyle,{color:color}]}>{text}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flexDirection: 'row',
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 80,
    marginVertical: Constants.BaseStyle.DEVICE_WIDTH / 100 * 3,
    // justifyContent: 'space-between',
    // marginBottom: Constants.BaseStyle.DEVICE_WIDTH / 100 * 3,

  },
  imgStyle:{
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 12,
    height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 12,
    borderRadius: Constants.BaseStyle.DEVICE_WIDTH / 100 * 6,
    borderWidth:1,
    resizeMode:'contain'
  },
  textStyle:{
        color: Constants.Colors.Black,
        fontSize: 16,
        fontWeight: "400",
        marginLeft: 10,
        width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 64,
    }
});
