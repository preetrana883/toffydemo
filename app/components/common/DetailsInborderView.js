'use strict';

import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View
} from 'react-native';

import Constants from '../../constants';

export default class DetailsInborderView extends Component{
  // Default render function
  render(){

    let { firstText, secondText, oneMore} = this.props;

    return(
      <View>
        <View style={styles.container}>
        {!this.props.more ?<View>
        <Text style={styles.DengueText}>{firstText}</Text>
            <Text style={styles.DengueSecondText}>{secondText}</Text>
        </View>: <Text style={styles.DengueSecondText}>{oneMore}</Text>}
            
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
        alignItems: 'center',
        borderWidth:.5,
        padding:10,
        marginLeft:10,
        marginTop:10,
        right:10,
        width: Constants.BaseStyle.DEVICE_WIDTH/100*40,
        height: Constants.BaseStyle.DEVICE_HEIGHT/100*8,
        justifyContent: 'center'
    },
    DengueText:{
        color: Constants.Colors.Black,
        fontSize: 16,
        fontWeight: "600"
    },
    DengueSecondText:{
        color: Constants.Colors.Black,
        fontSize: 14,
        fontWeight: "300",
        textAlign: "center",
    },
});
