import { createStackNavigator, createAppContainer } from 'react-navigation';

import DengueInsurranceOffer from 'app/screens/DengueInsurranceOffer';
import LocationInfo from 'app/screens/LocationInfo';
import ParentDetails from 'app/screens/ParentDetails';

const RNApp = createStackNavigator(
    {
        DengueInsurranceOffer: {
            screen: DengueInsurranceOffer,
            navigationOptions: { header: null, gesturesEnabled: false }
        },
        LocationInfo: {
            screen: LocationInfo,
            navigationOptions: { header: null, gesturesEnabled: false }
        },
        ParentDetails: {
            screen: ParentDetails,
            navigationOptions: { header: null, gesturesEnabled: false }
        }
    },
    {
        initialRouteName: 'DengueInsurranceOffer'
    }
);

export default createAppContainer(RNApp);
