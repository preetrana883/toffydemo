import { StyleSheet } from 'react-native';
import Constants from '../../constants';

const styles = StyleSheet.create({
  container: {
      flex: 1,
      // justifyContent: 'center',
      alignItems: 'center',
      margin: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4,
  },
  mrView:{
      flexDirection: 'row',
      marginVertical: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4,
  },
  locationText: {
      color: Constants.Colors.Black,
      fontSize: 22,
      fontWeight: "300",
      marginLeft: 10,
      marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
    },
    btnStyle: {
      backgroundColor: Constants.Colors.ButtonColor,
      marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 3,
      width: Constants.BaseStyle.DEVICE_WIDTH * 80 / 100,
      borderRadius: 5

    },
    textStyle: {
      fontSize: 18,
      fontWeight: '500'
    },
});

export default styles;
