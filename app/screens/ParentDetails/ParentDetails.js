import React, { Component } from 'react';
import { StyleSheet, View,Text, ScrollView,KeyboardAvoidingView } from 'react-native';
import * as loginActions from 'app/actions/loginActions';
import styles from './styles';
import { connect } from 'react-redux';
import Constants from '../../constants';
import UserWithText from '../../components/common/UserWithText';
import AddressInfo from '../../components/common/AddressInfo';
import CircleStatus from '../../components/common/CircleStatus';
import FormSubmitButton from '../../components/common/FormSubmitButton'

class ParentDetails extends Component {
    constructor(props) {
        super(props);
        this.state={
            fullname: '',
            email: '',
            mobile: '',
            mrSelected: true,
            mrsSelected: false,
            msSelected: false
        }
    }

    onSubmitPress(){
        let {fullname, email, mobile} = this.state;
        if(fullname == ''){
            alert("Please enter full name")
        } else if(email == ''){
            alert("Please enter email")
        } else if(mobile == ''){
            alert("Please enter mobile number")
        }
    }

    selectTitleFunc(val){
      switch(val){
        case 1: 
          this.setState({mrSelected:true,mrsSelected:false,msSelected:false})
          break;

        case 2: 
          this.setState({mrsSelected:true,mrSelected:false,msSelected:false})
          break;

        case 3: 
          this.setState({msSelected:true,mrSelected:false,mrsSelected:false})
          break;
      }
    }

    render() {
        let textValue = "Now please enter the name one of the parents with contact details like email and mobile number"
        return (
          <View style={styles.container}>
            <ScrollView>
              <UserWithText source={Constants.Images.user.userImg} 
              color={"gray"}
              text={textValue} />
              <View style={{width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 80,}}>
                <Text style={styles.locationText}>{"Father/Mother Details"}</Text>
              </View>

              <View style={styles.mrView}>
                <CircleStatus 
                  selectedTitle={this.state.mrSelected}
                  onPress={()=> this.selectTitleFunc(1)}
                selectedVal={this.state.selectedVal} text={"Mr"}/>
                
                <CircleStatus 
                  selectedTitle={this.state.mrsSelected}
                  onPress={()=> this.selectTitleFunc(2)}
                selectedVal={this.state.selectedVal} text={"Mrs"}/>
                
                <CircleStatus 
                  selectedTitle={this.state.msSelected}
                  onPress={()=> this.selectTitleFunc(3)}
                selectedVal={this.state.selectedVal} text={"Ms"}/>

              </View>
              <KeyboardAvoidingView behavior="padding">
                <AddressInfo placeholder={"Fullname"} 
                  width= {Constants.BaseStyle.DEVICE_WIDTH / 100 * 70}
                  onChangeText={(fullname) => this.setState({ fullname })}
                  keyboardType={"default"} value={this.state.state} />

                <AddressInfo placeholder={"Email"} 
                  width= {Constants.BaseStyle.DEVICE_WIDTH / 100 * 70}
                  onChangeText={(email) => this.setState({ email })}
                  keyboardType={"email-address"} value={this.state.state} />

                <AddressInfo placeholder={"10-digit mobile number"} 
                  mobile={true}
                  maxLength={10}
                  width= {Constants.BaseStyle.DEVICE_WIDTH / 100 * 60}
                  onChangeText={(mobile) => this.setState({ mobile })}
                  keyboardType={"phone-pad"} value={this.state.state} />

                <FormSubmitButton text={"Next"}
                  buttonStyle={styles.btnStyle}
                  _Press={() => this.onSubmitPress()}
                  textStyle={styles.textStyle} />
              </KeyboardAvoidingView>
              </ScrollView>
          </View>
        )
    }
}


function mapStateToProps() {
    return {};
}
function mapDispatchToProps() {
    return {};
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ParentDetails);
