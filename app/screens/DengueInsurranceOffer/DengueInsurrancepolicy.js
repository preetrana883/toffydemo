import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import styles from './styles';
import { connect } from 'react-redux';
import * as loginActions from 'app/actions/loginActions';
import Constants from '../../constants';
import PolicyTextCompo from '../../components/common/policyTextComp'
import DetailsInborderView from '../../components/common/DetailsInborderView'
import FormSubmitButton from '../../components/common/FormSubmitButton'

class DengueInsurrancepolicy extends Component {
  constructor(props) {
    super(props);
  }

  onPressBtn() {
    this.props.navigation.navigate("LocationInfo");
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.subView}>
          <Text style={styles.DengueText}>Dengue Insurance</Text>
          <Text style={[styles.DengueSecondText, { width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 70 }]}>Health insurance to cover all dengue related expenses.</Text>
          <View style={styles.flexRowView}>
            <PolicyTextCompo firstText={'1 year'} secondText={'Policy Duration'} />
            <PolicyTextCompo firstText={'$450'} secondText={'Annual Premium'} />
          </View>
        </View>
        <Image source={Constants.Images.user.Dengue} style={styles.dengueImgStyle} />
        <Text style={styles.coveredText}>What's Covered?</Text>
        <View style={styles.secSubView}>
          <View style={styles.secondBlock}>
            <DetailsInborderView firstText={'Hospitalisation'} secondText={'upto $25000'} />
            <DetailsInborderView firstText={'Diagnostic Tests'} secondText={'Covered'} />
          </View>
          <View style={styles.secondBlock}>
            <DetailsInborderView firstText={'Medicines'} secondText={'Covered'} />
            <DetailsInborderView oneMore={'1 More'} more={true} />
          </View>
        </View>
        <FormSubmitButton text={"See coverage details"}
          buttonStyle={styles.btnStyle}
          _Press={() => this.onPressBtn()}
          textStyle={styles.textStyle} />
        <View style={styles.footerStyle}>
          <Text style={styles.recommendStyle}>We also recommend you <Text style={[styles.recommendStyle,{fontWeight: '500'}]}>Sports Toffee</Text></Text>
        </View>
      </View>
    )
  }
}



function mapStateToProps(state) {

  return {
    user: state.loginReducer
  };
}
function mapDispatchToProps(dispatch) {
  return {
    onLogin: (un, pwd) => dispatch(loginActions.requestLogin(un, pwd))
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DengueInsurrancepolicy);
