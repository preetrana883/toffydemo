import { StyleSheet } from 'react-native';
import Constants from '../../constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subView: {
    alignItems: 'center',
    backgroundColor: Constants.Colors.PrimaryColor,
    paddingVertical: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 5
  },
  dengueImgStyle: {
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 30,
    height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 15,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 3,
    resizeMode:'contain',
    alignSelf:'center'
  },
  DengueText: {
    color: Constants.Colors.White,
    fontSize: 18,
    fontWeight: "600"
  },
  coveredText: {
    color: Constants.Colors.PureBlack,
    fontSize: 22,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 3,
    fontWeight: "300",
    textAlign:'center'
  },
  DengueSecondText: {
    color: Constants.Colors.White,
    fontSize: 16,
    fontWeight: "300",
    textAlign: "center",
    paddingVertical: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 1,
  },
  flexRowView: {
    flexDirection: 'row',
  },
  secSubView: {
    justifyContent: 'space-between',
    alignItems:'center',
    marginVertical: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2.5,
  },
  secondBlock: {
    flexDirection: 'row',
  },
  btnStyle: {
    backgroundColor: Constants.Colors.ButtonColor,
    width: Constants.BaseStyle.DEVICE_WIDTH * 50 / 100,
    alignSelf:'center',
    borderRadius:5
  },
  textStyle: {
    fontSize: 18
  },
  footerStyle:{
    borderWidth: .3,
    justifyContent:'flex-end',
    shadowOffset:{  width: 3,  height: 3,  },
    shadowColor: 'black',
    shadowOpacity: 1.0,
    marginHorizontal:Constants.BaseStyle.DEVICE_WIDTH/100*10,
    position:'absolute',
    bottom:0
  },
  recommendStyle:{
    fontSize: 17,
    padding:20,
  }
});

export default styles;
