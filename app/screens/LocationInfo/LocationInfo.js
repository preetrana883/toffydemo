import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import styles from './styles';
import * as loginActions from 'app/actions/loginActions';
import { connect } from 'react-redux';
import Constants from '../../constants';
import PolicydetailsInfo from '../../components/common/PolicydetailsInfo';
import UserWithText from '../../components/common/UserWithText';
import AddressInfo from '../../components/common/AddressInfo';

class LocationInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pincode: '',
      state: '',
      city: ''
    }
  }

  onNextPagePress(){
    let {pincode,state,city}= this.state;
    if(pincode==''){
      alert("Please enter pincode")
    } else if(state==''){
      alert("Please enter state")
    } else if(city==''){
      alert("Please enter city")
    } else {
      this.props.navigation.navigate("ParentDetails")
    } 
   
  }
  render() {
    console.log("this.props.reducer",this.props.user)
    let textValue = "Mythri, Please inter your residential details"
    return (
      <View style={styles.container}>
        <View style={styles.flexRowViewFirst}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Image source={Constants.Images.user.BackBtn} style={styles.imgStyleBack} />
          </TouchableOpacity>
          
          <View style={{ flexDirection: 'row' }}>
            <Image source={Constants.Images.user.Dengue} style={styles.imgStyleBack} />
            <View style={{ borderLeftWidth: 1, marginLeft: 10 }}></View>
            <View style={{ justifyContent: 'center' }}>
              <Text style={styles.betaStyle}>Beta</Text>
            </View>
          </View>
          
          <TouchableOpacity onPress={() => alert('Call')}>
            <Image source={Constants.Images.user.Call} style={styles.imgStyleBack} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          
          <View style={styles.subView}>
            <PolicydetailsInfo firstText={"Dengue Insurance"} secondText={ `${this.props.user.startYear} - ${this.props.user.endYear}`} />
            <Text style={styles.policyText}>POLICY NUMBER</Text>
            <Text style={styles.policyNoText}>{this.props.user.policyNumber}</Text>
            <PolicydetailsInfo firstText={this.props.user.username} secondText={this.props.user.email} />
          </View>
          
          <UserWithText source={Constants.Images.user.userImg} text={textValue} />
          
          <View style={styles.locationTextView}>
            <Image source={Constants.Images.user.location} style={styles.imgStyleBack} />
            <Text style={styles.locationText}>{"Residential Location"}</Text>
          </View>
          
          <AddressInfo placeholder={"Pin Code"} 
            width= {Constants.BaseStyle.DEVICE_WIDTH / 100 * 40}
            keyboardType={"number-pad"} maxLength={6}
            pincode={true} value={this.state.pincode} onChangeText={(pincode) => this.setState({ pincode })} />
          
          <AddressInfo placeholder={"State"} 
            width= {Constants.BaseStyle.DEVICE_WIDTH / 100 * 40}
            onChangeText={(state) => this.setState({ state })}
            keyboardType={"default"} value={this.state.state} />
          
          <AddressInfo placeholder={"City"} 
            width= {Constants.BaseStyle.DEVICE_WIDTH / 100 * 40}
            onChangeText={(city) => this.setState({ city })}
            keyboardType={"default"} value={this.state.city}
            onNextPress={() => this.onNextPagePress()}
            city={true} />
        </ScrollView>
      </View>
    )
  }
}


function mapStateToProps(state) {
  return {
    user: state.loginReducer
  };
}
function mapDispatchToProps() {
  return {};
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationInfo);
