import { StyleSheet } from 'react-native';
import Constants from '../../constants';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4,
  },
  locationTextView: {
    flexDirection: 'row',
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 80,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4,
    marginBottom: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
    // justifyContent: 'center',
    // borderWidth: 1
  },
  locationText: {
    color: Constants.Colors.Black,
    fontSize: 22,
    fontWeight: "300",
    marginLeft: 10
  },
  subView: {
    flex:1,
    alignItems: 'flex-start',
    elevation:5,
    marginVertical: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 90,
    backgroundColor: Constants.Colors.PrimaryColor,
    padding: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
    borderRadius: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
  },
  policyText: {
    color: Constants.Colors.White,
    fontSize: 14,
    fontWeight: "300",
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
  },
  policyNoText: {
    color: Constants.Colors.White,
    fontSize: 20,
    marginBottom: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 1.5,
  },
  flexRowViewFirst: {
    flexDirection: 'row',
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 85,
    // borderWidth: 1,
    justifyContent: 'space-between',
  },
  imgStyleBack: {
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 8.2,
    height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4,
    resizeMode:'contain'
  },
  betaStyle: {
    color: Constants.Colors.Black,
    fontSize: 14,
    marginLeft: 10
  }
});

export default styles;
